import axios from 'axios';

const apiToken = 'SlauvktIL11a0f5QoDXy';

const api = axios.create({
  baseURL: 'https://the-one-api.dev',
  headers: {
    Authorization: `Bearer ...`,
  },
});

export interface MovieData {
  academyAwardNominations: number;
  academyAwardWins: number;
  boxOfficeRevenueInMillions: number;
  budgetInMillions: number;
  name: string;
  rottenTomatoesScore: number;
  runtimeInMinutes: number;
  _id: string;
}

// @ts-ignore
export const getMovies = async (): Promise<MovieData[]> =>
  api
    .get('/v2/movie', {
      params: {
        limit: 5,
      },
    })
    .then((res) => {
      console.log('response', res.data);
    });

export interface QuoteData {
  dialog: string;
  movie: string;
  character: string;
  _id: string;
}
